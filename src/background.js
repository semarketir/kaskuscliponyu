chrome.webRequest.onBeforeRequest.addListener(function(details) {
	console.log('cliponyu detected : blocking!');
    return {
		cancel: true
	};
}, {
    urls: [
		'*://www.cliponyu.com/*', '*://cliponyu.com/*', '*://*.cliponyu.com/*',
		'*://www.doubleclick.net/*', '*://doubleclick.net/*', '*://*.doubleclick.net/*',
		'*://*/js/cliponyu/*.js'
	]
}, [ 'blocking' ] );